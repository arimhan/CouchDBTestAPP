package ulb.ac.couchDBTest.View;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.util.EntityUtils;
import ulb.ac.couchDBTest.Model.DBManager;

import javax.swing.*;
import javax.swing.plaf.DimensionUIResource;
import java.awt.*;
import java.util.List;

/**
 * Created by franc on 18-11-15.
 *
 */

public class DBCustomPanel extends JPanel {
    private JComboBox<String> dbList, type;
    private JTextArea requestText;
    private JTextArea resultText;
    private JTextField timerText;

    public DBCustomPanel(){
        JLabel timer = new JLabel("Time taken:");
        timerText = new JTextField();
        timerText.setEditable(false);
        timerText.setPreferredSize(new DimensionUIResource(60,20));

        dbList = new JComboBox<>();
        dbList.addItem("");
        dbList.setSelectedIndex(0);
        type = new JComboBox<>();
        type.addItem("PUT");
        type.addItem("GET");
        type.addItem("HEAD");
        type.setSelectedIndex(0);

        requestText =  new JTextArea();
        requestText.setLineWrap(true);
        requestText.setPreferredSize(new DimensionUIResource(200,200));
        resultText = new JTextArea();
        resultText.setLineWrap(true);
        resultText.setPreferredSize(new DimensionUIResource(500,300));
        resultText.setEditable(false);

        JButton send = new JButton("Execute");
        send.setPreferredSize(new DimensionUIResource(60,40));
        send.addActionListener(actionEvent -> {
            try {
                HttpResponse response;
                long startTime = 0;
                long stopTime = 0;
                response = null;
                switch ((String) type.getSelectedItem()) {
                    case "HEAD":
                        HttpHead head = new HttpHead(DBManager.getDb().getBaseUri() + "" + dbList.getSelectedItem() + requestText.getText().trim());
                        startTime = System.currentTimeMillis();
                        response = DBManager.getDb().executeRequest(head);
                        stopTime = System.currentTimeMillis();
                        break;
                    case "GET":
                        HttpGet get = new HttpGet(DBManager.getDb().getBaseUri() + "" + dbList.getSelectedItem() + requestText.getText().trim());
                        startTime = System.currentTimeMillis();
                        response = DBManager.getDb().executeRequest(get);
                        stopTime = System.currentTimeMillis();
                        break;
                    case "PUT":
                        HttpPut put = new HttpPut(DBManager.getDb().getBaseUri() + "" + dbList.getSelectedItem() + requestText.getText().trim());
                        startTime = System.currentTimeMillis();
                        response = DBManager.getDb().executeRequest(put);
                        stopTime = System.currentTimeMillis();
                        break;

                }

                long elapsedTime = stopTime - startTime;
                timerText.setText("" + elapsedTime + " ms");
                assert response != null;
                resultText.setText(EntityUtils.toString(response.getEntity(), "UTF-8"));
            }catch (Exception ex){
                ex.printStackTrace();
                JOptionPane.showMessageDialog(this,ex,"Error",JOptionPane.ERROR_MESSAGE);
            }
        });

        JButton refresh = new JButton("Refresh");
        refresh.setPreferredSize(new DimensionUIResource(60,40));
        refresh.addActionListener(actionEvent -> {
            List<String> alldb = DBManager.getDb().context().getAllDbs();
            dbList.removeAllItems();
            dbList.addItem("");
            for(String s : alldb){
                dbList.addItem(s);
            }
            dbList.setSelectedIndex(0);
        });

        this.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.HORIZONTAL;
        c.anchor = GridBagConstraints.PAGE_START;
        c.gridx = 0;
        c.gridy = 0;
        c.gridwidth = 1;
        c.weightx = 1;
        c.weighty = 1;
        this.add(refresh,c);

        c.gridx = 1;
        this.add(type,c);

        c.gridx = 2;
        this.add(dbList,c);

        c.gridx = 3;
        c.gridwidth = 3;
        c.weightx = 30;
        this.add(requestText,c);

        c.gridy = 1;
        c.gridx = 0;
        c.gridwidth = 1;
        c.weightx = 1;
        this.add(send,c);

        c.gridx = 2;
        this.add(timer,c);

        c.gridx = 3;
        this.add(timerText,c);

        c.gridx = 0;
        c.gridy = 2;
        c.weighty = 300;
        c.gridwidth = 5;
        c.fill = GridBagConstraints.BOTH;
        this.add(resultText,c);

        this.setVisible(true);
    }

}
