package ulb.ac.couchDBTest.View;

import com.google.gson.JsonObject;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpHead;
import org.lightcouch.DesignDocument;
import org.lightcouch.Response;
import ulb.ac.couchDBTest.Model.DBManager;

import javax.swing.*;
import javax.swing.plaf.DimensionUIResource;
import java.awt.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by franc on 17-11-15.
 *
 */
public class DBRequestPanel extends JPanel {
    private JTextArea mapText, reduceText;
    private JTextField timerText;
    private JComboBox<JsonObject> requestList;
    private JTextArea altview;
    private JScrollPane  scrollPanelRes;

    public DBRequestPanel(){
        JLabel timer = new JLabel("Time taken:");
        JLabel map = new JLabel("map:");
        JLabel reduce = new JLabel("reduce:");

        altview = new JTextArea();

        mapText = new JTextArea();
        mapText.setLineWrap(true);
        mapText.setPreferredSize(new DimensionUIResource(200,200));

        reduceText = new JTextArea();
        reduceText.setLineWrap(true);
        reduceText.setPreferredSize(new DimensionUIResource(200,200));

        timerText = new JTextField();
        timerText.setEditable(false);
        timerText.setPreferredSize(new DimensionUIResource(60,20));

        JTextField numberRowText = new JTextField();
        numberRowText.setEditable(false);
        numberRowText.setPreferredSize(new DimensionUIResource(60,20));

        scrollPanelRes = new JScrollPane();
        scrollPanelRes.setPreferredSize(new DimensionUIResource(200,40));

        requestList = new JComboBox<>();
        requestList.setPreferredSize(new DimensionUIResource(100,40));
        requestList.addActionListener(event -> {
                JsonObject item = (JsonObject) requestList.getSelectedItem();
                System.out.println(item + " " + item.get("id") + " " +  item.get("value").getAsJsonObject().get("rev"));
                DesignDocument design = DBManager.getDb().design().getFromDb(item.get("id").getAsString(), item.get("value").getAsJsonObject().get("rev").getAsString());
                DesignDocument.MapReduce mapreduce = design.getViews().get("request");

                mapText.setText(mapreduce.getMap());
                reduceText.setText(mapreduce.getReduce());
                // do something with object

        });

        JButton refresh = new JButton("Refresh");
        refresh.setPreferredSize(new DimensionUIResource(60,40));
        refresh.addActionListener(e ->{
            //get the list request on the server

           // DBManager.getDb().design().synchronizeAllWithDb();
            List<JsonObject> designList = DBManager.getDb().view("_all_docs").startKey("_design/").endKey("_design0").query(JsonObject.class);
            requestList.removeAllItems();

            for (JsonObject d : designList) {
                System.out.println(d);
                requestList.addItem(d);
            }
        });

        JButton execute = new JButton("Execute");
        execute.setPreferredSize(new DimensionUIResource(60,40));
        execute.addActionListener(e ->{
            try{
                //refresh the database
                DesignDocument designDocument = new DesignDocument();
                designDocument.setId("_design/customreqa");
                designDocument.setLanguage("javascript");

                DesignDocument.MapReduce request = new DesignDocument.MapReduce();
                request.setMap(mapText.getText());
                request.setReduce(reduceText.getText());

                Map<String, DesignDocument.MapReduce> view = new HashMap<>();
                view.put("request", request);

                designDocument.setViews(view);

                Response res = DBManager.getDb().design().synchronizeWithDb(designDocument);

                //execute the map reduce as an http request
                HttpHead head = new HttpHead(DBManager.getDb().getDBUri() + "_design/customreq/_view/request");


                long startTime = System.currentTimeMillis();
                //execute the map reduce
                    //List<JsonObject> list = DBManager.getDb().view("customreq/request")
                        //.query(JsonObject.class);
                HttpResponse response = DBManager.getDb().executeRequest(head);
                long stopTime = System.currentTimeMillis();
                long elapsedTime = stopTime - startTime;
                //create the table model
                //String listData [] = {"zcz","zecz","cezze"};
                //table = new JList<>(list.toArray(new JsonObject[list.size()]));
                //table.setPreferredSize(new DimensionUIResource(500,300));
                //table.setCellRenderer(new CustomListCell());
                altview.setPreferredSize(new DimensionUIResource(500,300));
                altview.setText(response.toString());
                altview.setEditable(false);
                altview.setLineWrap(true);
                timerText.setText("" + elapsedTime + " ms");
                //numberRowText.setText("" + list.size());

                scrollPanelRes.setViewportView(altview);   //display the table

                System.out.println(res.toString() + " " + res.getId() + " " + res.getRev());
                DBManager.getDb().remove(res.getId(), res.getRev());
                System.out.println("removed design doc");

                //table = new JTable(new JSONTableModel(allDocs));
                //table.setPreferredScrollableViewportSize(table.getPreferredSize());


            } catch(Exception ex){
                ex.printStackTrace();
                JOptionPane.showMessageDialog(this,ex,"Error",JOptionPane.ERROR_MESSAGE);
            }
        });

        this.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.HORIZONTAL;
        c.anchor = GridBagConstraints.PAGE_START;
        c.gridx = 0;
        c.gridy = 0;
        c.gridwidth = 4;
        c.weightx = 1;
        c.weighty = 1;
        this.add(requestList, c);

        c.gridx = 4;
        c.gridwidth = 1;
        this.add(refresh,c);

        c.gridx = 0;
        c.gridy = 1;
        this.add(map,c);

        c.gridx = 3;
        this.add(reduce,c);

        c.fill = GridBagConstraints.BOTH;
        c.gridy = 2;
        c.gridx = 0;
        c.weighty = 200;
        c.gridwidth = 2;
        this.add(new JScrollPane(mapText),c);
        c.gridx = 3;
        this.add(new JScrollPane(reduceText),c);

        c.anchor = GridBagConstraints.PAGE_END;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;
        c.gridy = 3;
        c.gridwidth = 1;
        c.weighty = 1;
        this.add(execute,c);

        c.gridx = 1;
        this.add(timer,c);

        c.gridx = 2;
        this.add(timerText,c);

        //c.gridx = 3;
        //this.add(numberRow,c);

        //c.gridx = 4;
        this.add(numberRowText,c);

        c.gridy = 4;
        c.gridx =0;
        c.weighty = 300;
        c.gridwidth = 5;
        c.fill = GridBagConstraints.BOTH;
        this.add(scrollPanelRes,c);
    }
}
