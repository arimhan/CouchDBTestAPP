package ulb.ac.couchDBTest.View;

import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import org.lightcouch.CouchDbProperties;
import org.lightcouch.Response;
import ulb.ac.couchDBTest.Model.DBManager;
import ulb.ac.couchDBTest.View.ActionListener.ConnectPopUp;

import javax.swing.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;



/**
 * Created by franc on 17-11-15.
 *
 */
public class MainWindow extends JFrame{
    public MainWindow(String s,int x, int y){
        try {
            // Set system look and feel
            UIManager.setLookAndFeel(
                    UIManager.getSystemLookAndFeelClassName());
        }catch (UnsupportedLookAndFeelException | ClassNotFoundException | InstantiationException | IllegalAccessException e) {
            // handle exception
        }

        /*
        Basic window creation
         */
        this.setTitle(s);
        this.setSize(x, y);
        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);

        /*
        Creating the menu bar
         */
        JMenuBar Menubar = new JMenuBar();

        JMenu Gestion = new JMenu("Gestion");
        JMenu Parameter = new JMenu("Parameters");

        DBViewPanel viewPanel = new DBViewPanel();

        JMenuItem Import = new JMenuItem("Import JSON");
        Import.addActionListener(e -> {
            File file;
            //get File name
            JFileChooser chooser = new JFileChooser();
            chooser.setDragEnabled(true);
            chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
            int returntype = chooser.showOpenDialog(this);
            if(returntype == JFileChooser.APPROVE_OPTION) {
                try{
                    file = chooser.getSelectedFile();//.getAbsolutePath();
                    JsonParser parser = new JsonParser();
                    List<Object> newDocs = new ArrayList<>();
                    JsonArray array = (parser.parse(new FileReader(file))).getAsJsonObject().get("docs").getAsJsonArray();
                    for(int i = 0; i < array.size(); i++){
                        newDocs.add(array.get(i));
                    }
                    //send to db
                    List<Response> bulkResponse = DBManager.getDb().bulk(newDocs, true);
                    //TODO wrap in a scrollpane
                    JScrollPane pane = new JScrollPane();
                    pane.add(new JLabel(bulkResponse.toString()));

                    viewPanel.refresh();
                    JOptionPane.showMessageDialog(null,pane,"OK",
                            JOptionPane.INFORMATION_MESSAGE
                    );
                }catch (FileNotFoundException ex){
                    JOptionPane.showMessageDialog(this,ex.getMessage(),"Error",JOptionPane.ERROR_MESSAGE);
                }
            }

        });

        JMenuItem Add = new JMenuItem("Create new DB");
        Add.addActionListener(e ->{
            String temp = JOptionPane.showInputDialog(null,"Enter the name of the new database","Creation of database",JOptionPane.PLAIN_MESSAGE);
            if(temp != null && temp.trim().equals("")){
                DBManager.getDb().context().createDB(temp);
            }

        });

        JMenuItem Delete = new JMenuItem("Delete the DB");
        Delete.addActionListener(e ->{
            if(JOptionPane.showConfirmDialog(null,"Are you sure?","Deletion of the database", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION){
                DBManager.getDb().context().deleteDB(DBManager.getProperties().getDbName(),"delete database");
            }
        });

        JMenuItem connect = new JMenuItem("Connect");
        connect.addActionListener(e ->{
            /*
            create pop up with connect info
             */
            try {
                CouchDbProperties prop = new ConnectPopUp(this).getProperties();
                if(prop== null) throw new Exception();
                DBManager.setDBProperties(prop);
                DBManager.connect();
                //viewPanel.refresh();
            }
            catch(Exception ex){
                JOptionPane.showMessageDialog(null,ex.getMessage(),"Error",JOptionPane.ERROR_MESSAGE);
            }
        });

        Gestion.add(Import);
        Gestion.add(Add);
        Gestion.add(Delete);
        Gestion.add(connect);

        Menubar.add(Gestion);
        Menubar.add(Parameter);

        this.setJMenuBar(Menubar);

                /*
        Add the tabbed panel
         */
        JTabbedPane tab = new JTabbedPane();
        tab.addTab("View Databases",null,viewPanel);
        tab.addTab("Request",null,new DBRequestPanel());
        tab.addTab("Custom Request",null, new DBCustomPanel());

        this.add(tab);
        /*
        Display the window
         */
        this.setVisible(true);
        //call the connect popup
        connect.doClick();
    }
}
