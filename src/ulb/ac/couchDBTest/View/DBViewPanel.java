package ulb.ac.couchDBTest.View;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import ulb.ac.couchDBTest.Model.CustomListCell;
import ulb.ac.couchDBTest.Model.DBManager;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.DimensionUIResource;
import java.awt.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by franc on 17-11-15.
 *
 */
public class DBViewPanel extends JPanel {
    private JList<JsonObject> table =  new JList<>();
    private JScrollPane sideView;
    private JScrollPane scrollPanel;
    private JButton refresh;
    private JTextField timerText;
    private JTextField numberRowText;

    public DBViewPanel(){
        JLabel timer = new JLabel("Time taken:");
        JLabel timer2 = new JLabel("Time taken:");
        JLabel numberRow = new JLabel("Number of record:");
        timerText = new JTextField();
        timerText.setEditable(false);
        timerText.setPreferredSize(new DimensionUIResource(60,20));

        numberRowText = new JTextField();
        numberRowText.setEditable(false);
        numberRowText.setPreferredSize(new DimensionUIResource(60,20));

        JTextField timerReqText = new JTextField();
        timerReqText.setEditable(false);
        timerReqText.setPreferredSize(new DimensionUIResource(60,20));

        scrollPanel = new JScrollPane();
        scrollPanel.setPreferredSize(new DimensionUIResource(200,40));

        sideView = new JScrollPane();//dynamic panel

        refresh = new JButton("Refresh");
        refresh.addActionListener(e ->{
            //refresh the database
            long startTime = System.currentTimeMillis();
            List<JsonObject> allDocs = DBManager.getDb().view("_all_docs").includeDocs(true).query(JsonObject.class);
            long stopTime = System.currentTimeMillis();
            long elapsedTime = stopTime - startTime;

            table = new JList<>(allDocs.toArray(new JsonObject[allDocs.size()]));
            table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            table.setCellRenderer(new CustomListCell());
            table.addListSelectionListener(listSelectionEvent -> {
                JsonObject o = table.getSelectedValue();
                JPanel p = new JPanel();
                p.setLayout(new BoxLayout(p,BoxLayout.Y_AXIS));
                Map<String,JComponent> viewMap = new HashMap<String,JComponent>();
                for(Map.Entry<String,JsonElement> entry : o.entrySet()){
                    //creer l'elemnt correspondant
                    JComponent t;
                    JsonElement value = entry.getValue();
                    JLabel l = new JLabel(entry.getKey() + ":");
                    if(value.isJsonPrimitive()){
                       t = new JTextField();
                        ((JTextField)t).setText(value.toString());
                    }
                    else if(value.isJsonArray()){
                        JsonObject temp [] = new JsonObject[value.getAsJsonArray().size()];
                        for(int i = 0; i < temp.length; i++){
                           temp[i] = value.getAsJsonArray().get(i).getAsJsonObject();
                        }
                        t =  new JList<>(temp);
                    }
                    else{
                        t = new JTextArea();
                        ((JTextArea)t).setText(value.toString());
                        t.setBorder(new EmptyBorder(10,5,5,0));
                    }
                    //ajouter a la map
                    viewMap.put(entry.getKey(),t);
                    //afficher
                    p.add(l);
                    p.add(t);
                    p.add(Box.createRigidArea(new DimensionUIResource(10,10)));
                }
                p.add(Box.createVerticalGlue());
                p.setVisible(true);
                sideView.setViewportView(p);

                //Add delete button

                //Add update button

            });

            //table = new JTable(new JSONTableModel(allDocs));
            //table.setPreferredScrollableViewportSize(table.getPreferredSize());
            //display the table
            //scrollPanel.removeAll();
            scrollPanel.setViewportView(table);

            //fill the timer and number of row
            timerText.setText("" + elapsedTime + " ms");
            numberRowText.setText("" + allDocs.size());
        });

        this.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.BOTH;
        c.anchor = GridBagConstraints.PAGE_START;
        c.gridx = 0;
        c.gridy = 0;
        c.gridwidth = 5;
        c.weightx = 1;
        c.weighty = 1000;
        this.add(scrollPanel, c);

        c.gridwidth = 2;
        c.gridx = 5;
        this.add(sideView,c);

        c.anchor = GridBagConstraints.PAGE_END;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;
        c.gridy = 1;
        c.gridwidth = 1;
        c.weighty = 1;
        this.add(refresh,c);

        c.gridx = 1;
        this.add(timer,c);

        c.gridx = 2;
        this.add(timerText,c);

        c.gridx = 3;
        this.add(numberRow,c);

        c.gridx = 4;
        this.add(numberRowText,c);

        c.gridx = 5;
        this.add(timer2,c);

        c.gridx = 6;
        this.add(timerReqText,c);
    }

    public void refresh() {
        refresh.doClick();
    }
}
