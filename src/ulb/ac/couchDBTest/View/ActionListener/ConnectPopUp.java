package ulb.ac.couchDBTest.View.ActionListener;

import org.lightcouch.CouchDbProperties;

import javax.swing.*;
import java.awt.*;

/**
 * Created by franc on 17-11-15.
 *
 */
public class ConnectPopUp {
    private JPanel panel;
    private JFrame parent;
    JTextField nameInput, hostInput, portInput, usernameInput, passwordInput;
    public ConnectPopUp(JFrame p){
        /*
        Create the panel
         */
        panel = new JPanel();
        parent = p;

        //TODO set size and format
        JLabel title = new JLabel("Enter the database information:"),
                name = new JLabel("Name:"),
                host = new JLabel("Host:"),
                port = new JLabel("port:"),
                username = new JLabel("Username:"),
                password = new JLabel("Password:");
        title.setPreferredSize(new Dimension(100,30));
        name .setPreferredSize(new Dimension(100,30));
        host.setPreferredSize(new Dimension(100,30));
        port.setPreferredSize(new Dimension(100,30));
        username.setPreferredSize(new Dimension(100,30));
        password.setPreferredSize(new Dimension(100,30));

        nameInput = new JTextField();
        nameInput.setPreferredSize(new Dimension(100,30));
        nameInput.setText("test");
        hostInput = new JTextField();
        hostInput.setPreferredSize(new Dimension(100,30));
        hostInput.setText("localhost");
        portInput = new JTextField();
        portInput.setPreferredSize(new Dimension(100,30));
        portInput.setText("5984");
        usernameInput = new JTextField();
        usernameInput.setPreferredSize(new Dimension(100,30));
        passwordInput = new JTextField();
        passwordInput.setPreferredSize(new Dimension(100,30));

        panel.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;
        c.gridy = 0;
        c.gridwidth = 2;
        c.weightx = 1;
        c.weighty = 1;
        panel.add(title,c);

        c.gridy = 1;
        c.gridwidth = 1;
        panel.add(name,c);

        c.gridx = 2;
        panel.add(nameInput,c);


        c.gridy = 2;
        c.gridx = 0;
        panel.add(host,c);

        c.gridx = 2;
        panel.add(hostInput,c);
//*
        c.gridy = 3;
        c.gridx = 0;
        panel.add(port,c);

        c.gridx = 2;
        panel.add(portInput,c);

        c.gridy = 4;
        c.gridx = 0;
        panel.add(username,c);

        c.gridx = 2;
        panel.add(usernameInput,c);

        c.gridy = 5;
        c.gridx = 0;
        panel.add(password,c);

        c.gridx = 2;
        panel.add(passwordInput,c);
        //*/

    }

    public CouchDbProperties getProperties(){
        CouchDbProperties prop = null;
        String name, host, username, password;
        int port;
        int result = JOptionPane.showConfirmDialog(parent,
                this.panel,
                "Connection",
                JOptionPane.OK_CANCEL_OPTION,
                JOptionPane.PLAIN_MESSAGE);
        if(result == JOptionPane.OK_OPTION) {
            name = nameInput.getText();
            host = hostInput.getText();
            port = Integer.parseInt(portInput.getText());
            username = (usernameInput.getText().trim().isEmpty())?null:usernameInput.getText().trim();
            password = (passwordInput.getText().trim().isEmpty())?null:passwordInput.getText().trim();

            prop = new CouchDbProperties(
                    name,
                    true,
                    "http",
                    host,
                    port,
                    username,
                    password
            );
        }
        return prop;

    }
}
