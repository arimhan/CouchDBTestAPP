package ulb.ac.couchDBTest.Model;

import org.lightcouch.CouchDbClient;
import org.lightcouch.CouchDbProperties;

/**
 * Created by franc on 17-11-15.
 *
 */
public class DBManager {
    private static CouchDbClient db;
    private static CouchDbProperties properties;

    public static void setDBProperties(CouchDbProperties p){
        if(db != null)db.shutdown();
        properties = p;
        connect();
    }

    public static void connect(){
        db = new CouchDbClient(properties);
    }

    public static CouchDbProperties getProperties() {
        return properties;
    }

    public static CouchDbClient getDb() {
        return db;
    }
}
