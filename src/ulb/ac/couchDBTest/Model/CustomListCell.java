package ulb.ac.couchDBTest.Model;

import javax.swing.*;
import java.awt.*;

/**
 * Created by franc on 18-11-15.
 */
public class CustomListCell extends JLabel implements ListCellRenderer {

    public CustomListCell() {
        setOpaque(true);
    }

    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        // Assumes the stuff in the list has a pretty toString
        setText(value.toString());

        // based on the index you set the color.  This produces the every other effect.
        if(isSelected) setBackground(Color.YELLOW);
        else if (index % 3 == 0) setBackground(Color.GRAY);
        else if( index % 3 == 1) setBackground(Color.LIGHT_GRAY);
        else setBackground(Color.WHITE);

        return this;
    }
}
