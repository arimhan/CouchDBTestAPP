package ulb.ac.couchDBTest.Model;

import com.google.gson.JsonObject;

import javax.swing.table.AbstractTableModel;
import java.util.List;

/**
 * Created by franc on 18-11-15.
 */
public class JSONTableModel extends AbstractTableModel {
    private List<JsonObject> list;
    private int maxsize;

    public JSONTableModel(List<JsonObject> l){
        list = l;
        int max = (list.get(0).entrySet()).size();
        for(int i = 0; i < list.size(); i++){
            if(list.get(i).entrySet().size() > max) max = list.get(i).entrySet().size();
        }
        maxsize = max;
    }

    @Override
    public String getColumnName(int column) {
        return ((list.get(0).entrySet().toArray()[column].toString()));
    }

    @Override
    public int getColumnCount() {
        return list.size();
    }

    @Override
    public int getRowCount() {
        return maxsize;
    }

    @Override
    public Object getValueAt(int row, int column) {
        String data;
        data = list.get(row).getAsJsonArray().get(column).toString();
        return data;
    }
}
